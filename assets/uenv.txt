# Check uenv_readme.txt for possible options
# You can add missing options by appending them here.

######## Defaults ########
# overlays=
#
# rootdev=mmcblk0p2
# rootlabel_retries=1
# rootfstype=ext4
# rootfs_fw=/lib/firmware/
#
# hekate_id=SWR-UBU
# reboot_action=bootloader
#
# hdmi_fbconsole=1
# bootargs_extra=
##########################

overlays=
rootdev=mmcblk0p2
hekate_id=SWR-UBU
reboot_action=bootloader
